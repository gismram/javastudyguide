package gmr.studyguide.java.tree.binary;

public class InorderAlgorithm {

	public Node minimum(Node n) {
		if (n == null || n.left == null)
			return n;
		return minimum(n.left);
	}

	public Node maximum(Node n) {
		if (n == null || n.right == null)
			return n;
		return maximum(n.right);
	}

	public Node sucessor(Node x) {
		if (x == null)
			return null;
		if (x.right != null) {
			return minimum(x.right);
		}
		Node y = x.parent;
		while (y != null && x == y.right) {
			x = y;
			y = y.parent;
		}
		return y;
	}

	public Node predecessor(Node node) {
		Node n = node;
		if (node == null)
			return null;
		if (node.left != null) {
			return maximum(node.left);
		}
		while (node.parent != null) {
			if (node == node.parent.right) {
				return node.parent;
			}
			node = node.parent;
		}
		return null;
	}

	public Node search(Node n, int value) {
		if (n == null)
			return n;
		System.out.print(n.data + " ");
		if (n.data == value) {
			return n;
		}
		if (value < n.data) {
			return search(n.left, value);
		} else {
			return search(n.right, value);
		}
	}

	public Node iterativeSearch(Node node, int value) {
		Node n = node;
		while (n != null && n.data != value) {
			System.out.print(n.data + " ");
			if (value < n.data) {
				n = n.left;
			} else {
				n = n.right;
			}
		}
		if (n != null && n.data == value) {
			System.out.print(n.data + " ");
			return n;
		}
		return null;
	}

	public void inorder(Node node) {
		if (node == null)
			return;
		inorder(node.left);
		System.out.print(node.data + " ");
		inorder(node.right);
	}

	public void preorder(Node node) {
		if (node == null)
			return;
		System.out.print(node.data + " ");
		preorder(node.left);
		preorder(node.right);
	}

	public void postorder(Node node) {
		if (node == null)
			return;
		postorder(node.left);
		postorder(node.right);
		System.out.print(node.data + " ");
	}

	public static void main(String[] args) {

		// Definir tree
		Node root = new Node(6);
		root.left = new Node(5, root);
		root.right = new Node(7, root);
		Node node2 = new Node(2, root.left);
		root.left.left = node2;
		Node node5 = new Node(5, root.left);
		root.left.right = node5;
		Node node8 = new Node(8, root.right);
		root.right.right = node8;

		InorderAlgorithm instance = new InorderAlgorithm();

		System.out.print("Inorder --> ");
		instance.inorder(root);
		System.out.println(" ");

		System.out.print("Preorder -->");
		instance.preorder(root);
		System.out.println(" ");

		System.out.print("Postorder -->");
		instance.postorder(root);
		System.out.println(" ");

		System.out.print("Search 8 -->");
		instance.search(root, 8);
		System.out.println(" ");

		System.out.print("Search iterative 8 -->");
		instance.iterativeSearch(root, 8);
		System.out.println(" ");

		Node s = instance.sucessor(node8);
		System.out.print("Sucessor de 8 -->" + (s == null ? null : s.data));
		System.out.println(" ");

		s = instance.predecessor(node5);
		System.out.print("Predecesor de 5 -->" + (s == null ? null : s.data));
		System.out.println(" ");
	}

}
