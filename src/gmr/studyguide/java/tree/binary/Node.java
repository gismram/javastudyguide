package gmr.studyguide.java.tree.binary;

public class Node {

	public int data;

	public Node parent;

	public Node left;

	public Node right;

	public Node() {
		data = 0;
	}

	public Node(int data) {
		this.data = data;
	}

	public Node(int data, Node parent) {
		this.data = data;
		this.parent = parent;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}

	public Node parent() {
		return this.parent;
	}

//	public String toString() {
//		return "data: " + this.data + ", parent: " + this.parent + ", left: " + this.left + ", right: " + right;
//	}
}
