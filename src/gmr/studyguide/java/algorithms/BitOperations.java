package gmr.studyguide.java.algorithms;

import java.util.ArrayList;

public class BitOperations {

	// le cambia el ultimo bit a 1
	public static int add(int x, int y) {
		int a, b;
		System.out.println("x inicial = " + Integer.toBinaryString(x));
		System.out.println("y inicial = " + Integer.toBinaryString(y));
		do {
			a = x & y;
			System.out.println("a = " + Integer.toBinaryString(a));
			b = x ^ y;
			System.out.println("b = " + Integer.toBinaryString(b));
			x = a << 1;
			System.out.println("x = " + Integer.toBinaryString(x));
			y = b;
			System.out.println("y = " + Integer.toBinaryString(y));
		} while (a != 0);
		System.out.println("res = " + b + ", " + Integer.toBinaryString(b));
		return b;
	}

	public static int negate(int x) {
		return add(~x, 1);
	}

	// le cambia el ultimo bit a 0
	public static int sub(int x, int y) {
		return add(x, negate(y));
	}

	/*
	 * You are given two 32-bit numbers, N and M, and two bit positions, i and
	 * j. Write a method to set all bits between i and j in N equal to M (e.g.,
	 * M becomes a substring of N located at i and starting at j). EXAMPLE:
	 * Input: N = 10000000000, M = 10101, i = 2, j = 6 Output: N = 10001010100
	 */
	public static void problem1(int n, int m, int i, int j) {

		System.out.println(Integer.toBinaryString(n));
		System.out.println(Integer.toBinaryString(m));
		// System.out.println(Long.toBinaryString(m << i));
		// System.out.println(Long.toBinaryString(n & (m << i)));
		// System.out.println(n & ~Long.MAX_VALUE);

		int max = ~0; /* All 1�s */

		System.out.println(Integer.toBinaryString(max));

		// 1�s through position j, then 0�s
		System.out.println(Integer.toBinaryString(1));
		System.out.println(Integer.toBinaryString((1 << j)));
		System.out.println(Integer.toBinaryString((((1 << j) - 1))));
		System.out.println(Integer.toBinaryString(max - ((1 << j) - 1)));
		int left = max - ((1 << j) - 1);
		System.out.println(Integer.toBinaryString(left));

		// 1�s after position i
		int right = ((1 << i) - 1);
		System.out.println(Integer.toBinaryString(right));

		// 1�s, with 0s between i and j
		int mask = left | right;
		System.out.println(Integer.toBinaryString(mask));

		// Clear i through j, then put m in there
		int r = (n & mask) | (m << i);

		System.out.println(Integer.toBinaryString(r));

	}

	/*
	 * Given a (decimal - e.g. 3.72) number that is passed in as a string, print
	 * the binary representation. If the number can not be represented
	 * accurately in binary, print �ERROR�
	 */
	public static String printBinary(String n) {
		int intPart = Integer.parseInt(n.substring(0, n.indexOf(".")));
		double decPart = Double.parseDouble(n.substring(n.indexOf("."), n.length()));
		String int_string = "";
		while (intPart > 0) {
			int r = intPart % 2;
			intPart >>= 1;
			int_string = r + int_string;
		}
		StringBuffer dec_string = new StringBuffer();
		while (decPart > 0) {
			if (dec_string.length() > 32)
				return "ERROR: " + dec_string;
			if (decPart == 1) {
				dec_string.append((int) decPart);
				break;
			}
			double r = decPart * 2;
			if (r >= 1) {
				dec_string.append(1);
				decPart = r - 1;
			} else {
				dec_string.append(0);
				decPart = r;
			}
		}
		return int_string + "." + dec_string.toString();
	}

	/*
	 * Write a function to determine the number of bits required to convert
	 * integer A to integer B. Input: 31, 14 Output: 2
	 */
	public static int bitSwapRequired(int a, int b) {
		int count = 0;
		for (int c = a ^ b; c != 0; c = c >> 1) {
			count += c & 1;
		}
		return count;
	}

	public static int swapOddEvenBits(int x) {
		System.out.println(Integer.toBinaryString(x));
		return (((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1));
	}

	/*
	public static int findMissing(ArrayList<Integer> array) {
		return findMissing(array, Integer.MAX_VALUE - 1);
	}

	public static int findMissing(ArrayList<Integer> input, int column) {
		if (column < 0) { // Base case and error condition
			return 0;
		}
		ArrayList<Integer> oddIndices = new ArrayList<Integer>();
		ArrayList<Integer> evenIndices = new ArrayList<Integer>();
		for (Integer t : input) {
			if (t.fetch(column) == 0) {
				evenIndices.add(t);
			} else {
				oddIndices.add(t);
			}
		}
		if (oddIndices.size() >= evenIndices.size()) {
			return (findMissing(evenIndices, column - 1)) << 1 | 0;
		} else {
			return (findMissing(oddIndices, column - 1)) << 1 | 1;
		}
	}
	*/
	
	public static void main(String[] args) {
		// System.out.println(add(7,1));
		// System.out.println(sub(8,1));
		// problem1(1000, 5, 2,4);
		// System.out.println(printBinary("600.9"));
		// System.out.println(bitSwapRequired(31, 14));
		System.out.println(Integer.toBinaryString(swapOddEvenBits(100)));
	}
}
