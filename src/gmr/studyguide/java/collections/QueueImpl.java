package gmr.studyguide.java.collections;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Spliterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class QueueImpl {

	public static void linkedlist() {
		// LinkedList como Queue
		LinkedList<Long> llist = new LinkedList<Long>();
		llist.add(10L);
		llist.add(1L);
		// llist.addFirst(1L);
		llist.addLast(5L);
		// llist.set(3, 20L);|
		llist.push(33L);
		llist.add(0, 2L);

		System.out.println("LinkedList: " + llist);
		System.out.println("Element: " + llist.element());
		System.out.println("Peek: " + llist.peek());
		System.out.println("First: " + llist.getFirst());
		System.out.println("Last: " + llist.getLast());
		System.out.println("Poll: " + llist.poll());
		System.out.println("LinkedList: " + llist);
		System.out.println("Pop: " + llist.pop());
		System.out.println("LinkedList: " + llist);
		llist.offer(100L);
		System.out.println("LinkedList: " + llist);

		// Iterar en orden
		Iterator<Long> it = llist.iterator();
		while (it.hasNext()) {
			System.out.println("Elem (orden normal): " + it.next());
		}

		it = llist.descendingIterator();
		while (it.hasNext()) {
			System.out.println("Elem (descendiente): " + it.next());
			// it.remove();
		}

		ListIterator<Long> it1 = llist.listIterator();
		while (it1.hasNext()) {
			System.out.println("Elem (listiterator) next: " + it1.next());
			// System.out.println("Elem (listiterator) prev: " +
			// it1.previousIndex());
		}
		while (it1.hasPrevious()) {
			System.out.println("Elem (listiterator) prev: " + it1.previous());
			// System.out.println("Elem (listiterator) prev: " +
			// it1.previousIndex());
		}

		Spliterator<Long> it2 = llist.spliterator();
		Consumer<Long> c = new Consumer<Long>() {

			@Override
			public void accept(Long t) {
				System.out.println("Accept: " + t);

			}

		};
		while (it2.tryAdvance(c)) {
			System.out.println("Elem (spltiterator): " + it2.estimateSize());
			// System.out.println("Elem (listiterator) prev: " +
			// it1.previousIndex());
		}

		Stream s = llist.parallelStream();
		System.out.println(s);
	}

	public static void priorityqueue() {
		Comparator<Integer> c = new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				int c = o1.compareTo(o2);
				System.out.println("o1: " + o1 + ", o2: " + o2 + ", c: " + c);
				return c;
			}
		};
		PriorityQueue<Integer> pqueue = new PriorityQueue<>(11, c);
		pqueue.add(0);
		pqueue.offer(3);
		pqueue.offer(4);
		pqueue.offer(2);
		System.out.println("PriorityQueue: " + pqueue);
		Iterator<Integer> it = pqueue.iterator();
		while (it.hasNext()) {
			System.out.println("Elem: " + it.next());
		}
		Object[] o = pqueue.toArray();
		Arrays.sort(o);
		for (Object long1 : o) {
			System.out.println("Ordenado con Arrays: " + long1);
		}
	}

	public static void linkedBlockinQueue() {
		class Producer implements Runnable {
			private final BlockingQueue<String> queue;
			private final int contador = 10;

			Producer(BlockingQueue<String> q) {
				queue = q;
			}

			public void run() {
				try {
					for (int i = 0; i < contador; i++) {
						queue.put(produce(i));
						//Thread.sleep(1);
					}
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}

			String produce(int c) {
				System.out.println("Producer(" + Thread.currentThread().getName() + "): produce" + c);
				return "produce" + c;
			}
		}

		class Consumer implements Runnable {
			private final BlockingQueue<String> queue;

			Consumer(BlockingQueue<String> q) {
				queue = q;
			}

			public void run() {
				try {
					while (true) {
						consume(queue.take());
						//Thread.sleep(10);
					}
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}

			void consume(String x) {
				System.out.println("Consumer(" + Thread.currentThread().getName() + "): " + x);
			}
		}
		System.out.println("LinkedBlockingQueue");
		
		Comparator<String> c = new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				int c = o1.compareTo(o2);
				System.out.println("o1: " + o1 + ", o2: " + o2 + ", c: " + c);
				return c;
			}
		};
		
		//BlockingQueue<String> q = new LinkedBlockingQueue<>();
		BlockingQueue<String> q = new ArrayBlockingQueue<String>(2, true);
		Producer p = new Producer(q);
		Consumer c1 = new Consumer(q);
		Consumer c2 = new Consumer(q);
		Consumer c3 = new Consumer(q);
		Consumer c4 = new Consumer(q);
		new Thread(p).start();
		new Thread(c1).start();
		new Thread(c2).start();
		new Thread(c3).start();
		new Thread(c4).start();
	}

	public static void main(String[] args) {

		// linkedlist();
		// priorityqueue();
		linkedBlockinQueue();

	}
}
