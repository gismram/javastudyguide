package gmr.studyguide.java.collections;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class MapImpl {

	static class RunnableMap implements Runnable {

		
		Map map;
		public RunnableMap(Map m) {
			map = m;
		}
		
		@Override
		public void run() {			
			for (int c = 0; c < 1000; c++) {
				map.put(Thread.currentThread().getName() + c, "thread" + c);
				//map.put("static" + c, "thread" + c);
			}
			System.out.println("Termino thread " + Thread.currentThread().getName() + " con elementos " + map.size());
		}
		
	}
	
	public static void concurrentMap(ConcurrentMap map) throws InterruptedException {
	
		RunnableMap r1 = new RunnableMap(map);
		Thread t1 = new Thread(r1, "t1"); 
		Thread t2 = new Thread(r1, "t2");
		Thread t3 = new Thread(r1, "t3");
		t1.start();
		t2.start();
		t3.start();		
		System.out.println("Mapa despues de ejecutar tres hilos: " + map.size());
		t1.join(); // espera a que termine el thread
		t2.join();
		t3.join();
		System.out.println("Mapa despues de terminar los tres hilos: " + map.size());
	}
	
	public static void main(String[] args) throws InterruptedException {

		// HashMap
		long i = System.currentTimeMillis();
		HashMap<String, String> map = new HashMap<String, String>(1, 0.25f);
		map.put("z", "vz");
		map.put("m", "vm");
		map.put("a", "va");
		long f = System.currentTimeMillis();
		System.out.println("HashMap: " + map + ": " + (f - i));

		// HashMap con 2 null values (solo puede almacenar uno nulo)
		i = System.currentTimeMillis();
		map = new HashMap<String, String>(1, 0.25f);
		map.put(null, "vz");
		map.put(null, "vm");
		map.put("a", "va");
		f = System.currentTimeMillis();
		System.out.println("HashMap con dos nulos: " + map + ": " + (f - i));

		// TreeMap
		i = System.currentTimeMillis();
		TreeMap<Long, String> treemap = new TreeMap<>();
		treemap.put(79099898L, "c");
		treemap.put(7L, "a");
		treemap.put(101L, "b");
		f = System.currentTimeMillis();
		System.out.println("TreeMap: " + treemap + ": " + (f - i));

		// LinkedHashMap con access order
		i = System.currentTimeMillis();
		// Con access order, a diferencia de los otros map, es un parametro mas,
		// por defecto es false
		LinkedHashMap<Long, String> linkmap = new LinkedHashMap<>(50, 1f, true);
		linkmap.put(79099898L, "c");
		linkmap.put(7L, "a");
		linkmap.put(101L, "b");
		f = System.currentTimeMillis();
		System.out.println("LinkedHashMap con access order previo a obtener la key 7: " + linkmap + ": " + (f - i));
		linkmap.get(7L);
		System.out.println("LinkedHashMap con access order al obtener key 7: " + linkmap + ": " + (f - i));

		// LinkedHashMap sin access order y limitando el # elems, quitando
		// siempre el primero de la lista para que se pueda introducir el nuevo
		i = System.currentTimeMillis();
		linkmap = new LinkedHashMap<Long, String>(3, 0.50f) {
			private static final long serialVersionUID = 1L;
			private static final int MAX_ENTRIES = 3;

			protected boolean removeEldestEntry(Map.Entry<Long, String> eldest) {
				return size() > MAX_ENTRIES;
			}
		};
		linkmap.put(1L, "c");
		linkmap.put(7L, "a");
		linkmap.put(101L, "b");
		f = System.currentTimeMillis();
		System.out.println("LinkedHashMap con 3 elem: " + linkmap + ": " + (f - i));
		linkmap.put(430L, "d");
		System.out.println("LinkedHashMap con 4 elem: " + linkmap + ": " + (f - i));
		linkmap.put(111L, "e");
		System.out.println("LinkedHashMap con 5 elem: " + linkmap + ": " + (f - i));

		// EnumMap
		i = System.currentTimeMillis();
		EnumMap<EnumTest, String> enummap = new EnumMap<>(EnumTest.class);
		enummap.put(EnumTest.UNO, "uno");
		enummap.put(EnumTest.DOS, "dos");
		enummap.put(EnumTest.TRES, "tres");
		f = System.currentTimeMillis();
		System.out.println("EnumMap: " + enummap + ": " + (f - i));

		// WeakHashMap
		i = System.currentTimeMillis();
		String a1 = "a0";
		String a2 = "a1";
		WeakHashMap<String, String> weakmap = new WeakHashMap<String, String>(16, 0.75f);
		weakmap.put(a1, a1);
		weakmap.put(a2, a2);
		for (int c = 2; c < 1000; c++) {
			weakmap.put("a" + c, "a" + c);
		}
		f = System.currentTimeMillis();
		System.out.println("WeakHashMap (1000a): " + weakmap.size() + ": " + (f - i));
		// System.gc();
		Runtime.getRuntime().gc();
		i = System.currentTimeMillis();
		for (int c = 0; c < 1000; c++) {
			weakmap.put("b" + c, "b" + c);
		}
		f = System.currentTimeMillis();
		System.out.println("WeakHashMap (1000b): " + weakmap.size() + ": " + (f - i));

		// IdentityHashMap
		i = System.currentTimeMillis();
		IdentityHashMap<String, String> idmap = new IdentityHashMap<String, String>();
		for (int c = 0; c < 1000; c++) {
			idmap.put("b" + c, "b" + c);
		}
		f = System.currentTimeMillis();
		System.out.println("IdentityHashMap: " + idmap.size() + ": " + (f - i));

		// IdentityHashMap con max elements
		i = System.currentTimeMillis();
		idmap = new IdentityHashMap<String, String>(10);
		for (int c = 0; c < 1000; c++) {
			idmap.put("b" + c, "b" + c);
		}
		f = System.currentTimeMillis();
		System.out.println("IdentityHashMap con max elems: " + idmap.size() + ": " + (f - i));
		
		// ConcurrentHashMap
		i = System.currentTimeMillis();
		ConcurrentHashMap<String, String> concmap = new ConcurrentHashMap<> ();
		for (int c = 0; c < 1000; c++) {
			concmap.put("b" + c, "b" + c);
		}
		f = System.currentTimeMillis();
		System.out.println("ConcurrentHashMap: " + concmap.size() + ": " + (f - i));
		concurrentMap(concmap);
		// Process
		// ProcessBuilder
	}

	public static enum EnumTest {
		UNO, DOS, TRES;
	}
}
