package gmr.studyguide.java.problems;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MaxValueWhileInputNumber {

	List<Integer> numbers = new ArrayList<>();

	int currentMaxValue = 0;

	public int maxValue(int value) {
		if (value > currentMaxValue) {
			currentMaxValue = value;
		}
		numbers.add(value);
		System.out.println("Max value: " + currentMaxValue);
		return currentMaxValue;
	}

	public static void main(String[] args) throws IOException {
		MaxValueWhileInputNumber instance = new MaxValueWhileInputNumber();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Integer:");
		try {
			while (true) {
				int i = Integer.parseInt(br.readLine());
				if (i == -1) {
					break;
				}
				instance.maxValue(i);
			}
			System.out.println("Numeros: " + instance.numbers);
			System.out.println("Max: " + instance.currentMaxValue);
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid Format!");
		}
	}
}
