package gmr.studyguide.java.problems;

import java.util.LinkedList;

public class DetectLoopLinkedList {

	static Node head; // head of list

	/* Linked list Node */
	static class Node {
		int data;
		Node next;

		Node(int d) {
			data = d;
			next = null;
		}
	}

	/* Inserts a new Node at front of the list. */
	public void push(int new_data) {
		/*
		 * 1 & 2: Allocate the Node & Put in the data
		 */
		Node new_node = new Node(new_data);

		/* 3. Make next of new Node as head */
		new_node.next = head;

		/* 4. Move the head to point to new Node */
		head = new_node;
	}

	public int detectLoop(Node node) {
		Node slow_p = node, fast_p = node;
		while (slow_p != null && fast_p != null && fast_p.next != null) { //) && fast_p.next.next != null) {
			slow_p = slow_p.next;
			fast_p = fast_p.next.next;//.next;
			if (slow_p == fast_p) {
				System.out.println("Found loop in " + slow_p.data);
				//removeLoop(slow_p, node);
				removeLoopBetter(slow_p, node);
				return 1;
			}
		}
		return 0;
	}
	
	 // Function to remove loop
    void removeLoop(Node loop, Node curr) {
        Node ptr1 = null, ptr2 = null;
 
        /* Set a pointer to the beging of the Linked List and
         move it one by one to find the first node which is
         part of the Linked List */
        ptr1 = curr;
        while (1 == 1) {
 
            /* Now start a pointer from loop_node and check if it ever
             reaches ptr2 */
            ptr2 = loop;
            while (ptr2.next != loop && ptr2.next != ptr1) {
                ptr2 = ptr2.next;
            }
 
            /* If ptr2 reahced ptr1 then there is a loop. So break the
             loop */
            if (ptr2.next == ptr1) {
                break;
            }
 
            /* If ptr2 did't reach ptr1 then try the next node after ptr1 */
            ptr1 = ptr1.next;
        }
 
        /* After the end of loop ptr2 is the last node of the loop. So
         make next of ptr2 as NULL */
        ptr2.next = null;
    }
    
 // Function to remove loop
    void removeLoopBetter(Node loop, Node head) {
        Node ptr1 = loop;
        Node ptr2 = loop;
 
        // Count the number of nodes in loop
        int k = 1, i;
        while (ptr1.next != ptr2) {
            ptr1 = ptr1.next;
            k++;
        }
 
        // Fix one pointer to head
        ptr1 = head;
 
        // And the other pointer to k nodes after head
        ptr2 = head;
        for (i = 0; i < k; i++) {
            ptr2 = ptr2.next;
        }
 
        /*  Move both pointers at the same pace,
         they will meet at loop starting node */
        while (ptr2 != ptr1) {
            ptr1 = ptr1.next;
            ptr2 = ptr2.next;
        }
 
        // Get pointer to the last node
        ptr2 = ptr2.next;
        while (ptr2.next != ptr1) {
            ptr2 = ptr2.next;
        }
 
        /* Set the next node of the loop ending node
         to fix the loop */
        ptr2.next = null;
    }

 // Function to print the linked list
    void printList(Node node) {
        while (node != null) {
            System.out.print(node.data + " ");
            node = node.next;
        }
    }
    
	/* Drier program to test above functions */
	public static void main(String args[]) {
		DetectLoopLinkedList llist = new DetectLoopLinkedList();
//		llist.push(20);
//        llist.push(4);
//        llist.push(15);
//        llist.push(10);
//        
		llist.head = new Node(50);
		llist.head.next = new Node(20);
		llist.head.next.next = new Node(15);
		llist.head.next.next.next = new Node(4);
		llist.head.next.next.next.next = new Node(10);
 
        // Creating a loop for testing 
		llist.head.next.next.next.next.next = llist.head.next.next;	

		llist.detectLoop(llist.head);
		
		llist.printList(llist.head);
	}

}
