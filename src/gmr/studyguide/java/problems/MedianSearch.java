package gmr.studyguide.java.problems;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class MedianSearch {

	LinkedList<Integer> numbers = new LinkedList<>();

	public void insertionSort(int number) {
		int p = numbers.size();
		if (p == 0) {
			numbers.add(number);
			return;
		}
		boolean swap = false;
		for (int i = p - 1; i >= 0; i--) {
			if (numbers.get(i) < number) {
				numbers.add(i + 1, number);
				swap = true;
				break;
			}
		}
		if (!swap) {
			numbers.add(0, number);
		}
		System.out.println("Insertion sort: " + numbers);
	}

	public int median() {
		if (numbers.size() == 1)
			return numbers.get(0);
		if (numbers.size() % 2 != 0) {
			// odd
			return numbers.get(numbers.size() / 2);
		} else {
			// even
			return (numbers.get((numbers.size() / 2) - 1) + numbers.get((numbers.size() / 2))) / 2;
		}
	}

	/*
	 * Perhaps we can use binary search on insertion sort to find location of
	 * next element in O(log n) time. Yet, we can�t do data movement in O(log n)
	 * time. No matter how efficient the implementation is, it takes polynomial
	 * time in case of insertion sort.
	 */
	public void z() {

	}

	public static void main(String[] args) throws IOException {
		MedianSearch instance = new MedianSearch();

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Integer:");
		try {
			while (true) {
				int i = Integer.parseInt(br.readLine());
				if (i == -1) {
					break;
				}
				instance.insertionSort(i);
				System.out.println("Median: " + instance.median());
			}
			System.out.println("Insertion sort final: " + instance.numbers);
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid Format!");
		}
	}
}
