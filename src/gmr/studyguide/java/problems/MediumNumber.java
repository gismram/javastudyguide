package gmr.studyguide.java.problems;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MediumNumber {

	List<Integer> numbers = new ArrayList<>();

	int currentMediumValue = 0;

	public double medium(int value) {
		int s = numbers.size();
		int total = 0;
		for (int i = 0; i < s; i++) {
			total += numbers.get(i);
		}
		System.out.println("Medium value: " + (total / s));
		return total / s;

	}

	// public static void main(String[] args) {
	// int[] numbers = { 5, 3, 9, 12, 1, 8, 12, 145, 142, 11, 2, 4, 14 };
	// System.out.println(medium(numbers));
	// }

	public static void main(String[] args) throws IOException {
		MediumNumber instance = new MediumNumber();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Integer:");
		try {
			while (true) {
				int i = Integer.parseInt(br.readLine());
				if (i == -1) {
					break;
				}
				instance.medium(i);
			}
			System.out.println("Numeros: " + instance.numbers);
			System.out.println("Medium: " + instance.currentMediumValue);
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid Format!");
		}
	}
}
