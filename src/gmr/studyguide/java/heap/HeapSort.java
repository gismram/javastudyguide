package gmr.studyguide.java.heap;

import java.util.ArrayList;
import java.util.List;

public class HeapSort {

	private int right(int x) {
		return x * 2 + 1;
	}

	private int left(int x) {
		return x * 2;
	}

	public void maxHeapify(List<Integer> A, int i, int heapsize) {
		int l = left(i);
		int r = right(i);
		int m = -1;
		if (l < heapsize && A.get(l) > A.get(i)) {
			m = l;
		} else {
			m = i;
		}
		if (r < heapsize && A.get(r) > A.get(m)) {
			m = r;
		}
		// Intercambia
		if (m != i) {
			int aux = A.get(i);
			A.set(i, A.get(m));
			A.set(m, aux);
			maxHeapify(A, m, heapsize);
		}
	}

	public void buildMaxHeap(List<Integer> A) {
		int heapsize = A.size();
		for (int i = (A.size() / 2 - 1); i >= 0; i--) {
			maxHeapify(A, i, A.size());
		}
	}

	public void heapSort(List<Integer> A) {
		buildMaxHeap(A);
		System.out.println("A: " + A);
		int j = 1;
		for (int i = A.size() - 1; i > 0; i --) {
			int tmp = A.get(0);
			A.set(0, A.get(i));
			A.set(i, tmp);
			System.out.println("A1: " + A);
			maxHeapify(A, 0, A.size() - (j++));
			System.out.println("A2: " + A);
		}
	}

	public static void main(String[] args) {
		List<Integer> A = new ArrayList<>();
		A.add(4);
		A.add(1);
		A.add(3);
		A.add(2);
		A.add(16);
		A.add(9);
		A.add(10);
		A.add(14);
		A.add(8);
		A.add(7);
		System.out.println("A before heapify: " + A);
		HeapSort instance = new HeapSort();
		//instance.buildMaxHeap(A);
		//System.out.println("A after heapify: " + A);
		instance.heapSort(A);
		System.out.println("A after heapsort: " + A);
	}
}
